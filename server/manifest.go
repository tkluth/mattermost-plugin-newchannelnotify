package main

var manifest = struct {
	Id      string
	Version string
}{
	Id:      "matttermost-plugin-newchannelnotify",
	Version: "0.9.2",
}
